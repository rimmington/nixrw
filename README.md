# nixrw

Re-write Nix store paths to be located at a different path. Potentially useful in an environment where `/nix/store` isn't writeable.

Uses a naïve path re-writing approach so may not work if store paths are obscured, for example in compressed archives.

Examples:

```bash
./nixrw /var/task $(nix-build '<nixpkgs>' -A hello) > hello.cpio
```

```nix
{ pkgs ? import <nixpkgs> {} }:

(pkgs.callPackage ./lib.nix {}).closureRelocated { rootPaths = [ pkgs.hello ]; newStoreDir = "/var/task"; }
```

{ bash, closureInfo, libarchive, replace, runCommand, writeScript }:

rec {
  closureRelocated = args: runCommand "closure-relocated" {} "${genClosureRelocated args} > $out";

  # A derivation that produces the relocated closure archive when executed
  genClosureRelocated = { rootPaths, newStoreDir, format ? "newc" }: writeScript "gen-closure-relocated" ''
    #!${bash}/bin/bash
    export PATH=${libarchive}/bin:${replace}/bin:$PATH
    bash ${./rw.sh} '${builtins.storeDir}' '${newStoreDir}' '${format}' < ${closureInfo { inherit rootPaths; }}/store-paths
  '';
}

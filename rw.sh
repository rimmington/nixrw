#!/usr/bin/env bash

set -euo pipefail

from="$1"
to="$2"
format="$3"

# Always overwrite at least 1 character of the hash to break dependencies
let minLen=${#from}+1
let maxLen=${#from}+10
if [[ ${#to} -gt $maxLen ]]; then
    >&2 echo "To path must be <= $maxLen characters long"
    exit 1
elif [[ ${#to} -lt $minLen ]]; then
    # Pad $to with /
    printf -v pad '/%.0s' $(seq 1 $minLen)
    to="$to$pad"
    to="${to:0:$minLen}"
fi

mapfile -t deps
let basei=${#to}+1
declare -a args
first=
for path in "${deps[@]}"; do
    if [ -z "$first" ]; then
        first=no
    else
        args+=("-a")
    fi
    args+=("$path" "$to/${path:$basei}")
done

>&2 echo "Closure has ${#deps[@]} derivations"

make_cpio() {
    find "${deps[@]}" -print0 | \
        bsdtar -cf - --files-from - --null -n -P --format newc --numeric-owner --uid 0 --gid 0 --no-acls --no-xattrs | \
        replace-literal -be "${args[@]}"
}

if [[ "$format" == "newc" ]]; then
    make_cpio
else
    make_cpio | bsdtar -cf - --format "$format" @-
fi
